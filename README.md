# vuejscreateproject

The following is a bash script to install [vue.js](https://vuejs.org/) with [node.js](https://nodejs.org/en/)  and [npm (node package manager) ](https://www.npmjs.com/dependencies) tested using [Ubuntu 18.04LTS](https://ubuntu.com/download/desktop) apt package manager.  This script installs  [vue/cli](https://cli.vuejs.org/) with nodejs and npm dependencies on [Ubuntu 18.04LTS](https://ubuntu.com/download/desktop) then opens [vue ui](https://cli.vuejs.org/dev-guide/ui-api.html#ui-files) the [Beta] Vue Project Managerfor vuejs project creation point of departure.

vue.js virgin install ending with instance of vue/cli beta create project http://localhost:8000/project/create

Many optional components are skipped. 
TODO learn how to skip the skipped optional install components
Final result is an open browswer instance running vuejs [Beta] Vue Project Manager on localhost:8080 for vuejs project creation user interface point of departure.
[screenshot](https://gitlab.com/vuejsinstall/vuejscreateproject/-/wikis/vuejs.install)

SEE [INSTALL.md](https://gitlab.com/vuejsinstall/vuejscreateproject/-/blob/master/INSTALL.md) for copy paste to terminal

 The following is a rendition of the first steps necessary to installing vue.js for web applications development. These instructions are somewhat specific to Ubuntu 18.04 LTS 64 bit. These instructions install vue.js using node.js and npm (node package manager) via the Ubuntu 18.04LTS apt package manager. An alternative nodejs install can be performed using the nodejs main page [download](https://nodejs.org/en/download/). Instructions for nvm (node version manager) installation are located below.
 
 SEE [INSTALL.md](https://gitlab.com/vuejsinstall/vuejscreateproject/-/blob/master/INSTALL.md) for copy paste to terminal
 
 The following includes the notes I took while creating the vuejsinstall bash script. For ease of use run the bash [INSTALL.md](https://gitlab.com/vuejsinstall/vuejscreateproject/-/blob/master/INSTALL.md) script.

On Ubuntu 18.04 LTS nodejs and npm are part of the apt ecosystem

sudo apt-get update && sudo apt-get install nodejs && sudo apt-get install npm

# check version of nodejs
nodejs -v
# check version of npm (node package manager)
npm -v

The problem... with a fresh install of Ubuntu 18.04 64bit using apt this returns nodejs v8.10.0
There are several ways to install node.js. The terminal solution using npm is

# update npm to the lastest with global
sudo npm install npm@latest -g

#then use npm to update node
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
# To reset the command location hash either start a new shell, or execute PATH="$PATH"
PATH="$PATH"

# you should see something like
installed : v12.16.1 (with npm 6.13.4)
npm -v and node -v confirm this

# then use npm to install vue/cli

npm install -g @vue/cli
vue --version
# globally upgrade the vue package
npm update -g @vue/cli

# start project using User Interface graphically
vue ui


While at the time of this writing nodejs current is at version 13
[nodejs current](https://nodejs.org/en/download/current/)

# [nvm](https://github.com/nvm-sh/nvm)
# nvm (node version manager) can be used to change versions of nodejs
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
export NVM_DIR=”$HOME/.nvm”
[ -s “$NVM_DIR/nvm.sh” ] && \. “$NVM_DIR/nvm.sh”
[ -s "$NVM_DIR/bash_completion" ] && \.   "$NVM_DIR/bash_completion"
nvm — version
nvm use v10.4.1

February 15, 2020
Vue CLI requires Node. js version 8.9 or above (8.11. 0+ recommended). I did not test vue.js with the default install with of node.js v8.10.0 opting instead for version v12.16.1

The vue code is super tiny and has two versions, production and development. You can also call vue.js externally while developing your prototype.

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


or download from [https://vuejs.org/v2/guide/installation.html](https://vuejs.org/v2/guide/installation.html) 

For production link to a specific version number and build to avoid chaos:
For example current
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
For example previous
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.4"></script>

exploration
https://webpack.js.org/
http://browserify.org/



[vue cli](https://cli.vuejs.org/guide/)


notes on editors
I like https://atom.io/ gedit and the terminal

# check your local user export PATH
sudo nano ~/.bashrc
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"